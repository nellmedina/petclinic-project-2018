# petclinic-project-2018

Planned creation of various versions of [Spring Petclinic](https://github.com/spring-projects/spring-petclinic) app to be familiar with cool frontend and backend frameworks.


```scala
Version 1:
> angular 1
> nodejs
> mongodb

Version 2:
> change to angular 6

Version 3:
> change to reactjs

Version 4:
> change backend
> use spring boot and postgres

Version 5:
> go mobile
> use Android and Kotlin

Version 6:
> go Hybrid
> use react native

Version 7:
> then try ionic framework


// TIMELINE:
sep (app)
- angular6
- graphql
- material design

- nodejs
- mongodb

- springboot
- postgres

- jhipster gateway (zuul)
- jhipster registry (eureka, config)
- jhipster console (elk)

- ab benchmarking
- kafka messaging

oct (infra)
 - terraform (IAC)
 - mongodb atlas
 - nexus repository
 - kubernetes

nov (mobile)
 - ionic pwa
 - firebase

dec (mobile)
```